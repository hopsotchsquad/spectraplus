(function ($) {
	console.log('Loading SpectraPlus');

	var SpectraPlusSettings = {};
	var SpectraPlus = window.SpectraPlus = {};
	if (localStorage.SpectraPlusSettings) SpectraPlusSettings = JSON.parse(localStorage.SpectraPlusSettings);

	// finished loading
	console.log("SpectraPlus loaded");
	app.send('/cmd spectraplus'); // let the server know we're using SpectraPlus

}).call(this, jQuery);
